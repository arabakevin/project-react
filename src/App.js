import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

import { Link } from 'react-router'

class App extends Component {
  render() {
    return (
      <div className="App">
   <ul>
     <li>
      <Link to="/home" className="btn btn-link">Home</Link>
     </li>
     <li>
     <Link to="/about" className="btn btn-link">About</Link>
     </li>
     <li>
     <Link to="/contact" className="btn btn-link">Contact</Link>
     </li>
   </ul>
   {this.props.children}
</div>
    );
  }
}

export default App;
