import React from 'react';
import Content from './Content';

export default class User extends React.Component{
  constructor (){
    super();

    this.state = {
      data:[
        {
          fullname: 'Dupont Gerard',
          age: 24,
          job: 'Developer analyst',
        },
        {
          fullname: 'Imagine William',
          age: 36,
          job: 'Business Analyst',
        },
        {
          fullname: 'Araba Sarah',
          age: 24,
          job: 'Operation',
        }
      ]
    }
  }

  render(){
    return (
      <div>
      {this.state.data.map((dynamicComponent, i) => <Content
        key = {i} componentData = {dynamicComponent}/>)}
      </div>
    );
  }
}
