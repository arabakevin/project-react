import React from 'react';
import RefFocus from './RefFocus';
import FirstPage from './FirstPage';

export default class About extends React.Component{
  render(){
    return(
      <div>
        <h1> ABOUT... </h1>
        <RefFocus />
        <FirstPage />
      </div>
    );

  }

}
