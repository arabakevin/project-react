import React from 'react';

export default class Content extends React.Component{
  render(){
    return (
      <div class="card">
        <img src="/static/media/logo.5d5d9eef.svg" alt="Avatar"></img>
        <div class="container">
          <h4><b>{this.props.componentData.fullname}</b></h4>
          <h2> {this.props.componentData.age}</h2>
          <p>{this.props.componentData.job}</p>
        </div>
      </div>
    );
  }
}
