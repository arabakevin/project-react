import React from 'react';

export default function CoolComponent({adjective = 'Cool'}) {
  return <p>Youpi So {adjective} !</p>
}
