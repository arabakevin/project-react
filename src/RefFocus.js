import React from 'react';
import ReactDOM from 'react-dom';

export default class RefFocus extends React.Component {
  constructor(props) {
   super(props);

    this.state = {
       data: '',
       previous_value: '',
    }
    this.updateState = this.updateState.bind(this);
    this.clearInput = this.clearInput.bind(this);
 };
 updateState(e) {
    this.setState({data: e.target.value});
 }
 clearInput() {
    this.setState({previous_value: this.state.data})
    this.setState({data: ''});
    ReactDOM.findDOMNode(this.refs.myInput).focus();
 }
 render() {
   const old_value = this.state.previous_value;
    return (
       <div>
          <input value = {this.state.data} onChange = {this.updateState}
             ref = "myInput"></input>
          <button onClick = {this.clearInput}>CLEAR</button>
          <h4>{this.state.data}</h4>
          <p> previous data: {old_value}</p>
       </div>
    );
 }
}
