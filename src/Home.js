import React from 'react';
import First from './First';

export default class Home extends React.Component{
  render(){
    return(
      <div>
        <h1> HOME... </h1>
        <p> We use bootstrap </p>
        <button type="button" class="btn">Basic</button>
        <button type="button" class="btn btn-primary">Primary</button>
        <button type="button" class="btn btn-secondary">Secondary</button>
        <button type="button" class="btn btn-success">Success</button>
        <button type="button" class="btn btn-info">Info</button>
        <button type="button" class="btn btn-warning">Warning</button>
        <button type="button" class="btn btn-danger">Danger</button>
        <button type="button" class="btn btn-dark">Dark</button>
        <button type="button" class="btn btn-light">Light</button>
        <button type="button" class="btn btn-link">Link</button>
        <p></p>
        <First adjective="awesome" />
        <First />
      </div>
    );

  }

}
