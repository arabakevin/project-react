import React from 'react';
import ReactDOM from 'react-dom';

export default class FirstPage extends React.Component {
  // All React component classes that have a constructor should start it with a super(props) call
 constructor(props) {
      super(props);

      this.state = {
         data: [],
         state_nil: true,
      }

      // bind take in relation with the class where is the method
      this.setStateHandler = this.setStateHandler.bind(this);
      this.findDomNodeHandler = this.findDomNodeHandler.bind(this);
      this.returnZero = this.returnZero.bind(this);
   };

      setStateHandler() {
      var item = "setState..."
      var myArray = this.state.data.slice();
      myArray.push(item);
      this.setState({state_nil: false});
      this.setState({data: myArray});
   };

    returnZero() {
      this.setState({data: []});
   };

      findDomNodeHandler() {
      var myDiv = document.getElementById('myDiv');
      ReactDOM.findDOMNode(myDiv).style.color = 'green';
      ReactDOM.findDOMNode(myDiv).innerHTML = "new content";

  };

  render() {
    // `const` is a signal that the identifier won’t be reassigned.
    // `let`, is a signal that the variable may be reassigned, such as a counter in a loop, or a value swap in an algorithm. It also signals that the variable will be used only in the block it’s defined in, which is not always the entire containing function.
    // `var` is now the weakest signal available when you define a variable in JavaScript.

    let rep_list_present;
    rep_list_present = (this.state.state_nil ? 'We dont have element in this list' : 'we have in min one element');
    return (
            <div>
              <button onClick = {this.returnZero}>Restart</button>
              <button onClick = {this.setStateHandler}>SET STATE</button>
              <h4>State Array: {this.state.data}</h4>
              <br/>
              <p> {rep_list_present} </p>
              <br/>
              <br/>
              <button onClick = {this.findDomNodeHandler}>FIND DOME NODE</button>
              <br/>
              <br/>
              <div id = "myDiv">NODE</div>
              <br/>
            </div>

    );
  }

}
